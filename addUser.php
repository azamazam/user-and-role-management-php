<?php include('navbar.php');

/**
 * Created by PhpStorm.
 * User: DeskTop
 * Date: 3/6/2018
 * Time: 7:59 PM
 */
$id = "";
$userName = "";
$name = "";
$email = "";
$password = "";
$country = "";
$isAdmin = "";
$createdby = "";
if (isset($_REQUEST["savebtn"]) == true) {
    $userName = $_REQUEST["userName"];
    $name = $_REQUEST["name"];
    $email = $_REQUEST["email"];
    $password = $_REQUEST["password"];
    $country = $_REQUEST['countries'];
    $isAdmin = $_REQUEST["isAdmin"];
    $createdby = $_SESSION["userid"];
    $id = $_REQUEST["id"];
    if (isset($_REQUEST["id"]) == false || !($id>=1)) {
        $sql = "insert into users(login, password, name, email, countryid,  createdby,isAdmin) VALUES ('$userName','$password','$name','$email','$country','$createdby','$isAdmin')";

        //Step-2: Execute SQL Query
        if (mysqli_query($conn, $sql))
            echo "User Added successfully";
        else
            echo "cant be ";

    }
}
if (isset($_REQUEST["id"])) {
    $id = $_REQUEST["id"];
    if (isset($_REQUEST["savebtn"]) == true) {

        $sql = "update users set login='$userName' , name='$name' ,password='$password',email='$email',countryid='$country',isAdmin='$isAdmin' where id='$id' ";
        if (mysqli_query($conn, $sql))
            echo "updated ";
    } else {

        $sql = "select login,name,email,countryid,isAdmin ,password from users where id='$id'";
        $result = mysqli_query($conn, $sql);
        $recordsFound = mysqli_num_rows($result);
        if ($recordsFound > 0) {
            $row = mysqli_fetch_assoc($result);

            $userName = $row["login"];
            $name = $row["name"];
            $email = $row["email"];
            $country = $row["countryid"];
            $isAdmin = $row["isAdmin"];
            $password = $row["password"];
        }
    }
}
?>
<html>
<head>
    <script>


        //      var savebtn = document.getElementById("savebtn");

        //    var table = document.getElementById("userTable");

            var isUserNameVerify = true;
            var isNameVerify = true;
            var isEmailVerify = true;
            var isPasswordVerify = true;
            var isCountryVerify = true;
            var isCityVerify = true;

        /*
                    userName.onfocusout = checkUserName;
                    name.onfocusout = checkName;
                    email.onfocusout = checkEmail;
                    password.onfocusout = checkPassword;
                    confirmPassword.onfocusout = matchPassword;
        */
            function checkUserName() {
                var userName = document.getElementById("userName");

                if (userName.value.trim(' ') == 0) {
                    //       alert("please  Enter User Name");
                    userName.style.border = "1px solid red";
                    isUserNameVerify = false;
                }
                else if (userName.value.indexOf(' ') >= 0) {
                    //         alert("Enter User Name without spaces");
                    userName.style.border = "1px solid red";
                    isUserNameVerify = false;
                }
                else {
                    userName.style.border = "1px solid black";
                    isUserNameVerify = true;
                }

            }

            function checkName() {
                var name = document.getElementById("Name");

                if (name.value.trim(' ') == 0) {
                    //            alert("please  Enter Name");
                    name.style.border = "1px solid red";
                    isNameVerify = false;
                }
                else {
                    name.style.border = "1px solid black";
                    isNameVerify = true;
                }
            }


            function validateEmail(emailvalue) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(emailvalue);
            }

            function checkEmail() {
                var email = document.getElementById("email");
                if (!validateEmail(email.value)) {
                    //          alert("please  provide email");
                    email.style.border = "1px solid red";
                    isEmailVerify = false;
                }
                else {
                    email.style.border = "1px solid black";
                    isEmailVerify = true;
                }
            }


            function checkPassword() {

                var password = document.getElementById("password");


                if (password.value.length == 0) {
                    //      alert("Please Enter password");
                    password.style.border = "1px solid red";
                    isPasswordVerify = false;
                }
                else {
                    password.style.border = "1px solid black";
                    isPasswordVerify = true;      //why  why error  if  remove this   ?
                }
            }


            function matchPassword() {

                var password = document.getElementById("password");
                var confirmPassword = document.getElementById("confirmPassword");

                if (password.value != confirmPassword.value || password.value.length == 0) {
                    //  alert("Both Passwords  does not  match");
                    password.style.border = "1px solid red";
                    confirmPassword.style.border = "1px solid red";
                    isPasswordVerify = false;
                    password.value = confirmPassword.value = '';
                }
                else {
                    password.style.border = "1px solid black";
                    confirmPassword.style.border = "1px solid black";
                    isPasswordVerify = true;
                }
            }


            function checkCountry() {
                var countrycmb = document.getElementById("cmbCountries");

                if (countrycmb.value == "--select--") {
                    countrycmb.style.border = "1px solid red";
                    isCountryVerify = false;
                }
                else {
                    countrycmb.style.border = "1px solid black";
                    isCountryVerify = true;
                }
            }

        function isvalidData() {

            checkUserName();
                checkName();
                checkEmail();
                checkPassword();
                matchPassword();
                checkCountry();


                if (isUserNameVerify &&
                    isNameVerify &&
                    isEmailVerify &&
                    isCountryVerify &&
                    isCityVerify &&
                    isPasswordVerify) {
                    return true;
                } else {
                    alert("Some values are missing");
                    return false;
                }
        }

    </script>
</head>
<body
">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form onsubmit="return isvalidData();" method="post" action="addUser.php">
            <legend align="center">User Management</legend>
            <input hidden name="id" id="id" value="<?php echo $id ?>">
            <div class="form-group">
                <label>UserName:*</label>
                <input type="text" name="userName" id="userName" value="<?php echo $userName ?>" class="form-control">
            </div>
            <div class="form-group"><label>Name:*</label>
                <input type="text" id="Name" name="name" class="form-control" value="<?php echo $name ?>">
            </div>
            <div class="form-group"><label>Email:*</label>
                <input type="email" id="email" name="email" class="form-control" value="<?php echo $email ?>">
            </div>
            <div class="form-group">
                <label>Password:*</label>
                <input type="password" id="password" name="password" class="form-control"
                       value="<?php echo $password ?>">
            </div>
            <label>Confirm Password:</label>
            <input type="password" id="confirmPassword" name="confirmPassword" class="form-control"
                   value="<?php echo $password ?>">
            <div class="form-group"><label>Country:</label>
                <select id="cmbCountries" name='countries' class="form-control">
                    <option>--select--</option>
                    <?php $sql = "SELECT id,name FROM countries";

                    //Step-2: Execute SQL Query
                    $result = mysqli_query($conn, $sql);

                    //Step-3: Get count of result
                    $recordsFound = mysqli_num_rows($result);
                    if ($recordsFound > 0) {

                        //Step-4: Iterate row by row
                        while($row = mysqli_fetch_assoc($result)) {

                            $id = $row["id"];
                            $name = $row["name"];

                            //Step-5: Display values
                            if ($id == $country)
                                echo "<option value=' $id'  selected>$name</option>" . "<br>";
                            else
                                echo "<option value=' $id' >$name</option>" . "<br>";
                        }
                    }?>
                </select>
            </div>
            <div class="form form-group ">
                <label>IsAdmin</label> <input type="checkbox" name="isAdmin" class="checkbox checkbox-inline" <?php if($isAdmin=="on"){ ?> checked <?php } ?> >
            </div>
            <div class="form-group">
                <input type="submit" id="savebtn" name="savebtn" value="Save"
                       class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>

</div>

</body>
</html>