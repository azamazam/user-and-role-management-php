<?php require('functions.php');
require ('conn.php');
session_start();
if (isset($_SESSION["user"])==false )
    header('location:index.php');

    ?>
<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <script src="bootstrap/js/jquery.js" type="text/javascript"></script>
    <script src='bootstrap/js/bootstrap.min.js' type="text/javascript"></script>
    <script src='bootstrap/js/jquery.min.js' type="text/javascript"></script>


    <title>Assignment 2</title>

</head>
<body>
<header>
    <h1 id='name'> Security Manager </h1>
    <nav id='main-nav' style="border-top: 6px solid darkcyan; " class='navbar navbar-inverse navbar-responsive'>
        <div class="container-fluid">
            <ul class="nav navbar-nav ">
                <li class="active">
                    <a href="welcomeAdmin.php" class="">Home</a>
                </li>
                <li>
                    <a class='dropdown-toggle' href="#"
                       data-toggle="dropdown">
                        <span class="caret"></span> User Management
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="addUser.php">Add User</a>
                        </li>
                        <li>
                            <a href="showUser.php">Show All </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class='dropdown-toggle' href="#"
                       data-toggle="dropdown">
                        <span class="caret"></span> Role Management
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="addRole.php">Add Role </a>
                        </li>
                        <li>
                            <a href="showRole.php">Show All</a>
                        </li>
                    </ul>
                </li >
                <li>
                    <a class='dropdown-toggle' href="#"
                       data-toggle="dropdown">
                        <span class="caret"></span> Permission Management
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="addPermission.php">Add Permission </a>
                        </li>
                        <li>
                            <a href="showPermission.php">Show All</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class='dropdown-toggle' href="#"
                       data-toggle="dropdown">
                        <span class="caret"></span> Role-Permission Assignment
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="addRolePermission.php">Add Role Permission </a>
                        </li>
                        <li>
                            <a href="showRolePermission.php">Show All</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class='dropdown-toggle' href="#"
                       data-toggle="dropdown">
                        <span class="caret"></span> User Role Assignment
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="adduserRole.php">Add User Role  </a>
                        </li>
                        <li>
                            <a href="showUserRole.php">Show All</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="loginHistory.php">Login History  </a>
                </li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="active">
                    <a class='dropdown-toggle ' href="#"
                       data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION["user"] ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="index.php?logout=true"> logout</a>
                        </li>

                    </ul>
                </li>

            </ul>
        </div>
    </nav>



</body>
</html>