<?php include("navbar.php");
/**
 * Created by PhpStorm.
 * Permission: DeskTop
 * Date: 3/9/2018
 * Time: 10:51 PM
 */
?>
<html>
<head>
    <script>
        function Main() {
            //      alert("Permissions Mangement ");
            var savebtn = document.getElementById("savebtn");
            var permissionName = document.getElementById("permissionName");
            var description = document.getElementById("description");
            var permissionTable = document.getElementById("permissionTable");

            var permissions = SecurityManager.GetAllPermissions();

            var isPermissionNameVerify = true;
            var permissionData = {};

            permissionName.onfocusout = checkPermissionName;
            savebtn.onclick = savePermission;

            tablefill();

            function tablefill() {

                for (permission in permissions) {
                    var row = document.createElement("tr");
                    permissionTable.appendChild(row);

                    var datainrow = document.createElement("td");
                    datainrow.innerText = permissions[permission]["ID"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = permissions[permission]["permissionName"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = permissions[permission]["description"];
                    row.appendChild(datainrow);


                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >edit</a>"
                    datainrow.setAttribute("id", permissions[permission]["ID"]);
                    datainrow.setAttribute("onclick", "editPermission(" + permissions[permission]["ID"] + ");")
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >delete</a>"
                    datainrow.setAttribute("id", permissions[permission]["ID"]);
                    datainrow.setAttribute("onclick", "deletePermission(" + permissions[permission]["ID"] + ");")
                    row.appendChild(datainrow);

                }
            }


            function checkPermissionName() {
                if (permissionName.value.trim() == 0) {
                    //       alert("please  Enter permission Name");
                    permissionName.style.border = "1px solid red";
                    isPermissionNameVerify = false;
                }
                else {
                    permissionName.style.border = "1px solid black";
                    isPermissionNameVerify = true;
                }
                var f = false;
                for (permission in permissions) {
                    if (permissionName.value == permissions[permission]["permissionName"] && permissions[permission]["ID"] != permissionData["ID"]) {
                        f = true;
                        break;
                    }
                }
                if (f) {
                    permissionName.style.border = "1px solid red";
                    isPermissionNameVerify = false;
                    //   alert("Permission  already  exists");
                }

            }

            function savePermission() {
                checkPermissionName();


                if (isPermissionNameVerify) {
                    permissionData["permissionName"] = permissionName.value;
                    permissionData["description"] = description.value;

                    //    alert(JSON.stringify(permissionData));
                    SecurityManager.SavePermission(permissionData, loadPage, err);

                } else alert("Some value are missing");

            }

            editPermission = function (permissionId) {
                permissionData = SecurityManager.GetPermissionById(permissionId);
                permissionName.value = permissionData["permissionName"];
                description.value = permissionData["description"];


            }
            deletePermission = function (permissionId) {
                var isdelete = confirm("Are You sure  You want  to delete Permission " + (SecurityManager.GetPermissionById(permissionId))["permissionName"]);
                if (isdelete)
                    SecurityManager.DeletePermission(permissionId, loadPage, err);

            }

            function loadPage() {
                window.location.href = "addPermission.php";
            }

            function err() {
                alert("there is  some  problem ");
            }

        }


    </script>
</head>
<body onload="Main();">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">Permissions</legend>
            <div class="form-group">
                <label>Permission Name:*</label>
                <input type="text" id="permissionName" class="form-control">
            </div>
            <div class="form-group"><label>Description:</label>
                <input type="text" id="description" class="form-control">
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" id="permissionTable">
            <legend align="center">Permission Table</legend>
            <tr>
                <th>ID</th>
                <th>Permission Name</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

