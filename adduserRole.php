<?php include("navbar.php");
/**
 * Created by PhpStorm.
 * User: DeskTop
 * Date: 3/9/2018
 * Time: 10:51 PM
 */


?>
<html>
<head>
    <script>


        function Main() {
            //      alert("Users Role Management ");
            var savebtn = document.getElementById("savebtn");
            var userNamecmb = document.getElementById("userName");
            var roleNamecmb = document.getElementById("roleName");
            var userRoleTable = document.getElementById("userRoleTable");

            var users = SecurityManager.GetAllUsers();
            var roles = SecurityManager.GetAllRoles();
            var usersRoles = SecurityManager.GetAllUserRoles();

            var userRoleData = {};
            var isUserNameVerify = true;
            var isRoleNameVerify = true;
            var isUserRoleVerify = true;

            userNamecmb.onchange = checkUserName;
            roleNamecmb.onchange = checkRoleName;
            savebtn.onclick = saveUserRole;

            tablefill();
            fillcomboboxes();

            function tablefill() {

                for (rp in usersRoles) {
                    var row = document.createElement("tr");
                    userRoleTable.appendChild(row);

                    var datainrow = document.createElement("td");
                    datainrow.innerText = usersRoles[rp]["ID"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = usersRoles[rp]["userName"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = usersRoles[rp]["roleName"];
                    row.appendChild(datainrow);


                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >edit</a>"
                    datainrow.setAttribute("onclick", "editUserRole(" + usersRoles[rp]["ID"] + ");")
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >delete</a>"
                    datainrow.setAttribute("onclick", "deleteUserRole(" + usersRoles[rp]["ID"] + ");")
                    row.appendChild(datainrow);
                }
            }


            function fillcomboboxes() {
                for (user  in  users) {
                    var op = document.createElement("option");
                    op.innerText = users[user].userName;
                    userNamecmb.appendChild(op);
                }

                for (p in  roles) {
                    var op = document.createElement("option");
                    op.innerText = roles[p].roleName;
                    roleNamecmb.appendChild(op);
                }
            }


            function checkUserName() {
                if (userName.value == "--select--") {
                    //       alert("please  Enter user Name");
                    userNamecmb.style.border = "1px solid red";
                    isUserNameVerify = false;
                }
                else {
                    userNamecmb.style.border = "1px solid black";
                    isUserNameVerify = true;
                }

            }

            function checkRoleName() {

                if (roleNamecmb.value == "--select--") {
                    //       alert("please  Enter user Name");
                    roleName.style.border = "1px solid red";
                    isRoleNameVerify = false;
                }
                else {
                    roleNamecmb.style.border = "1px solid black";
                    isRoleNameVerify = true;
                }
            }
            function checkuserRole() {
                isUserRoleVerify = true;
                for (rp in usersRoles) {
                    if (usersRoles[rp].userName == userNamecmb.value &&
                        usersRoles[rp].roleName == roleNamecmb.value &&
                        usersRoles[rp].ID != userRoleData.ID) {

                        userNamecmb.style.border = "1px solid red";
                        roleNamecmb.style.border = "1px solid red";
                        isUserRoleVerify = false;
                    }

                }
            }

            function saveUserRole() {
                checkUserName();
                checkRoleName();
                checkuserRole();
                if (isUserNameVerify && isRoleNameVerify && isUserRoleVerify) {
                    userRoleData["userName"] = userNamecmb.value;
                    userRoleData["roleName"] = roleNamecmb.value;

                    //      alert(JSON.stringify(userRoleData));
                    SecurityManager.SaveUserRole(userRoleData, loadPage, err);

                } else alert("Some Thing went wrong \n Fileds Are empty or This User has this role already");

            }

            editUserRole = function (userId) {
                userRoleData = SecurityManager.GetUserRoleById(userId);
                userNamecmb.value = userRoleData["userName"];
                roleNamecmb.value = userRoleData["roleName"];

            }
            deleteUserRole = function (userId) {
                var isdelete = confirm("Are You sure  You want  to delete User " + (SecurityManager.GetUserRoleById(userId))["userName"]);
                if (isdelete)
                    SecurityManager.DeleteUserRole(userId, loadPage, err);
            }

            function loadPage() {
                window.location.href = "adduserRole.php";
            }

            function err() {
                alert("there is  some  problem ");
            }

        }

    </script>
</head>
<body onload="Main();">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">Users-Role-Assignment</legend>
            <div class="form-group">
                <label>User Name:*</label>
                <select id="userName" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group"><label>Role:</label>
                <select id="roleName" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" id="userRoleTable">
            <legend align="center">User Role Table</legend>
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Role</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

