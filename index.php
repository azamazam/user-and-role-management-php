<?php require("conn.php");
session_start();
if (isset($_REQUEST["logout"]) == true) {
    session_destroy();
    header('location:index.php');
}
?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <script src="bootstrap/js/jquery.js" type="text/javascript"></script>
    <script src='bootstrap/js/bootstrap.min.js' type="text/javascript"></script>
    <script src='bootstrap/js/jquery.min.js' type="text/javascript"></script>

    <title>Assignment 2</title>
    <script>
        function Main() {

            var adminloginbtn = document.getElementById('loginbtn');


            adminloginbtn.onclick = function () {
                var UserName = document.getElementById("userName");
                var Password = document.getElementById("password");
                if (UserName.value.trim(' ') == 0 && Password.value.length == 0) {
                    alert("Please Enter User Name And Password ");

                    return false;
                }
                if (UserName.value.trim(' ') == 0) {
                    alert("Please Enter User Name");
                    return false;
                }
                if (Password.value.length == 0) {
                    alert("Please Enter password");
                    return false;
                }
            }
        }
    </script>

</head>
<?php
$errormsg = "";
if (isset($_REQUEST["loginbtn"]) == true) {
    $uname = $_REQUEST["userName"];
    $pswd = $_REQUEST["password"];
    $sql = "select id,isAdmin from users where login='$uname' and password='$pswd'  ";
    $result = mysqli_query($conn, $sql);
    $recordsFound = mysqli_num_rows($result);
    if ($recordsFound > 0) {   // record  verfied
        $row = mysqli_fetch_assoc($result);
        $_SESSION["user"] = $uname;
        $id = $row["id"];
        $_SESSION["userid"] = $id;
        $_SESSION["isAdmin"] = $row["isAdmin"];
        $localIP = getHostByName(getHostName());
        $sql = "insert into loginhistory (userid, login, machinip) VALUES('$id','$uname','$localIP') ";
        mysqli_query($conn, $sql);
        header('location:welcomeAdmin.php');
    } else $errormsg = "Username or password is incorrect";
}

?>

<body onload="Main();">
<h1>Security Manager </h1>
<hr>


<form class="form col-lg-4 col-lg-offset-1" method="post">
    <legend>Login</legend>
    <div class="form-group">
        <label>UserName :</label>
        <input id="userName" name="userName" type="text" class="form-control">
    </div>
    <div class="form-group">
        <label>Password :</label>
        <input id="password" name="password" type="password" class="form-control">

    </div>
    <span style="color: red"><?php echo $errormsg ?></span>
    <div class="form-group">

        <input type="submit" value="login" name="loginbtn" id="loginbtn" class=" btn btn-block btn-success"/>
    </div>

</form>

</body>

</html>